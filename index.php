<?php

require_once("animal.php");
require_once("frog.php");
require_once("Ape.php");

$sheep = new Animal("shaun");
echo $sheep->name . "<br>"; // "shaun"
echo $sheep->legs . "<br>"; // 2
echo $sheep->cold_blooded . "<br><br>"; // false

$kodok = new Frog("buduk");
echo $kodok->name . "<br>";
echo $kodok->legs . "<br>";
echo $kodok->jump() . "<br><br>";

$sungokong = new Ape("kera sakti");
echo $sungokong->name . "<br>";
echo $sungokong->legs . "<br>";
$sungokong->yell(); // "Auooo"

?>